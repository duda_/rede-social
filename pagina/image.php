<?php   session_start();?>
<?php include('components/conexao.php'); ?>
<html lang="pt-br">
 <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Socialdive - Perfil</title>
<link rel="shortcut icon" href="../imagens/icone_da_socialdive.ico" />
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <head>
<style "type= text/css">
            body {
                color: #black;

                margin-left: auto;
                margin-right: auto;
                width: 90%;
                height:100%;

                 }  

                #imagem {
                width: 700px;
                height: 900px;
                }

                #texto {
                position: center;
                margin-top: -850px;
                }

</style>
<body>
<?php

if(isset($_SESSION['usuario'])){ 
?>

<h3 class="mt-3"><b>Alterar dados:</b></h3>
<h4></h4>
<div class="card">
<form enctype="multipart/form-data" action="uploadimg.php" method="post">
<input type="hidden" name="MAX_FILE_SIZE" value="99999999"/>
    <div><input name="imagem" type="file"/></div>
    <div><input type="submit" value="Salvar"/></div>
</form>



<?php }
else { ?>
<div id="erro">
<div class="card" style="width: 100%;">
  <div class="card-body">
    <h5 class="card-title">Uma conta inexistente não pode ser alterada!</h5>
    <h6 class="card-subtitle mb-2 text-muted">Login não foi realizado.</h6>
    <p class="card-text">Clique <a href='login.php'>aqui</a> para fazer o Login.</p>
  </div>
  </div>
</div><?php }?>
</body>
 <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script> </body>