<html lang="pt-br">
 <head>
    <?php include('components/conexao.php'); ?>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Socialdive - Cadastro</title>
    <link rel="shortcut icon" href="../imagens/icone_da_socialdive.ico" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  </head>
    <style "type= text/css">
     body  {
                font-color: white;
                font-size: 14px;
                width: 100%;
                background-image: url("../imagens/cadastro2.JPG");
                background-repeat: no-repeat;

            }
            #titulo {
                width: 520px;
                margin-right: 10px;
                margin-top: -10;
            }
            input[type=text]{outline:none; color: white;}
            input[type=date]{outline:none; color: white;}
            input[type=E-mail]{outline:none; color: white;}
            input[type=Password]{outline:none; color: white;}
            input[type=radio]{outline:none;}
            #input0
            {
                background: transparent;
                width: 500px;
                border-bottom-color: #ffffff;
                border-top-color: transparent;
                border-left-color: transparent;
                border-right-color: transparent;
                border-width: thin;
                border-radius: 0px;
                margin-left: 400px;
                margin-top: -100px;
            }
            #input1
            {
                background: transparent;
                width: 500px;
                border-bottom-color: #ffffff;
                border-top-color: transparent;
                border-left-color: transparent;
                border-right-color: transparent;
                border-width: thin;
                border-radius: 0px;
                margin-left: 400px;
                margin-top: -50px;
            }
            #input2
            {
                background: transparent;
                width: 500px;
                border-bottom-color: #ffffff;
                border-top-color: transparent;
                border-left-color: transparent;
                border-right-color: transparent;
                border-width: thin;
                border-radius: 0px;
                margin-left: 400px;
                margin-top: 0px;
            }
            #input3
            {
                background: transparent;
                width: 500px;
                border-bottom-color: #ffffff;
                border-top-color: transparent;
                border-left-color: transparent;
                border-right-color: transparent;
                border-width: thin;
                border-radius: 0px;
                margin-left: 400px;
                margin-top: 25px;
            }
            #input4
            {
                background: transparent;
                width: 379px;
                border-bottom-color: #ffffff;
                border-top-color: transparent;
                border-left-color: transparent;
                border-right-color: transparent;
                border-width: thin;
                border-radius: 0px;
            }
            ::-webkit-input-placeholder {
                color: white;
                font-size: 14px;
                font-family: Arial;
                font-weight: -100;
                
            }    
            input {
               border:1px solid white;
               background-color: transparent;
               border-radius: 10px;
               font-color: white;
               width: 200px;
               margin-top: 20px;
            }   
            input:hover {
                background-color: #EECDD8;
            }
            #button {
                outline: none;
            }
            #login {
                margin-top: 100px;
            }
            a:hover {
             text-decoration: underline; 
             color: black;  
             }
            a {
                color: white;
            }
            #nascimento {
                width: 500px;
                margin-left: 400px;
                margin-top: -5px;
                margin-right: -90px;
            }
            #radio {
                width: 500px;
                margin-left: 400px;
                margin-top: 30px;
            }
            #salvar {
            color: white 
            }

     </style>
    <body>
        <center>
        <img src="../imagens/titulo.png" id="titulo"/></center>
        <form action="salvar_usuario.php" class="needs-validation" validate>
            <input type="text" id="input0" placeholder="Nome de usuário" name="nick" required>
            <input type="text" id="input1" placeholder="Nome completo" name="nome" required>
            <input type="E-mail" id="input2" placeholder="E-mail" name="email" required>
            <input type="Password" id="input3" placeholder="Senha" name="senha" required>
<!--            <p>Foto: <input type="file" name="image" /></br> -->
            <p>
            <div id="nascimento"><center><font color="white" size="2px">
            Data de nascimento:
            <input type="date" id="input4" name="data_nasc" required>
            </div></center></font>
            <p>     
            <center><div id="salvar">
            <input id="button" type="submit" value="Salvar"></div>
        </form>
        <div id="login"> Já possui uma conta? <a id="loginlink" href="login.php">Login</a></div></center></font>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </body>
</html>